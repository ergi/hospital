package com.hospital.Hospital.api;

import com.hospital.Hospital.Exception.HospitalCustomException;
import com.hospital.Hospital.controller.HospitalController;
import com.hospital.Hospital.models.Appointment;
import com.hospital.Hospital.models.User;
import com.hospital.Hospital.models.Error;
import io.swagger.annotations.ApiParam;
import javax.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author erginushi
 */

@CrossOrigin(origins = "*", maxAge = 3600)
@Controller
public class HospitalApiController implements HospitalApi {
    
    @Override
    public ResponseEntity<Object> createAppointment (
            @ApiParam(value = "The Appointment to be created" ,required=true )  @Valid @RequestBody Appointment appointment
    ) {
        Object response; 
        Integer statusCode = 200;
        try {
            System.out.println("Request: " + appointment);
            response = HospitalController.createAppointment(appointment);
        } catch (HospitalCustomException e) {
            System.out.println("Custom Exception: " + e);
            statusCode = Integer.parseInt(e.getFaultObject().getCode());
            response = e.getFaultObject();
        } catch (Exception ex) {
            System.out.println("General exception: " + ex);
            statusCode = 500;
            Error error = new Error();
            String message = "Something went wrong";
            error.setCode(statusCode.toString());
            error.setMessage(message);
            response = error;
        }
        
        System.out.println("Response: " + response);
        return new ResponseEntity<>(response, HttpStatus.valueOf(statusCode));
    }
    
    @Override
    public ResponseEntity<Object> getAllAppointments(
           @ApiParam(value = "Doctor id") @Valid @RequestParam(value = "doctorId", required = false) String doctorId,
           @ApiParam(value = "User id") @Valid @RequestParam(value = "date_lt", required = false) String date_lt,
           @ApiParam(value = "User id") @Valid @RequestParam(value = "date_gt", required = false) String date_gt
    ) {
        Object response; 
        Integer statusCode = 200;
        try {
            System.out.println("Request: " + doctorId);
            response = HospitalController.getAllAppointments(doctorId, date_lt, date_gt);
        } catch (HospitalCustomException e) {
            System.out.println("Custom Exception: " + e);
            statusCode = Integer.parseInt(e.getFaultObject().getCode());
            response = e.getFaultObject();
        } catch (Exception ex) {
            System.out.println("General exception: " + ex);
            statusCode = 500;
            Error error = new Error();
            String message = "Something went wrong";
            error.setCode(statusCode.toString());
            error.setMessage(message);
            response = error;
        }
        
        System.out.println("Response: " + response);
        return new ResponseEntity<>(response, HttpStatus.valueOf(statusCode));
    }
    
    @Override
    public ResponseEntity<Object> deleteAppointment(
        @ApiParam(value = "Doctor id") @Valid @PathVariable(value = "doctorId", required = true) String doctorId,
        @ApiParam(value = "Appointment id") @Valid @PathVariable(value = "id", required = true) String appointmentId
    ) {
        Object response; 
        Integer statusCode = 200;
        try {
            System.out.println("Request: " + doctorId);
            response = HospitalController.deleteAppointment(doctorId, appointmentId);
        } catch (HospitalCustomException e) {
            System.out.println("Custom Exception: " + e);
            statusCode = Integer.parseInt(e.getFaultObject().getCode());
            response = e.getFaultObject();
        } catch (Exception ex) {
            System.out.println("General exception: " + ex);
            statusCode = 500;
            Error error = new Error();
            String message = "Something went wrong";
            error.setCode(statusCode.toString());
            error.setMessage(message);
            response = error;
        }
        
        System.out.println("Response: " + response);
        return new ResponseEntity<>(response, HttpStatus.valueOf(statusCode));
    }
    
    @Override
    public ResponseEntity<Object> createUser(
            @ApiParam(value = "The User to be created" ,required=true )  @Valid @RequestBody User user
    ) {
        Object response; 
        Integer statusCode = 200;
        try {
            System.out.println("Request: " + user);
            response = HospitalController.createUser(user);
        } catch (HospitalCustomException e) {
            System.out.println("Custom Exception: " + e);
            statusCode = Integer.parseInt(e.getFaultObject().getCode());
            response = e.getFaultObject();
        } catch (Exception ex) {
            System.out.println("General exception: " + ex);
            statusCode = 500;
            Error error = new Error();
            String message = "Something went wrong";
            error.setCode(statusCode.toString());
            error.setMessage(message);
            response = error;
        }
        
        System.out.println("Response: " + response);
        return new ResponseEntity<>(response, HttpStatus.valueOf(statusCode));
    }
    
    @Override
    public ResponseEntity<Object> getAllUsers(
        @ApiParam(value = "The receptionist" ,required=true )  @Valid @RequestParam String user
    ) {                
        Object response; 
        Integer statusCode = 200;
        try {
            System.out.println("Request: " + user);
            response = HospitalController.getAllUsers(user);
        } catch (HospitalCustomException e) {
            System.out.println("Custom Exception: " + e);
            statusCode = Integer.parseInt(e.getFaultObject().getCode());
            response = e.getFaultObject();
        } catch (Exception ex) {
            System.out.println("General exception: " + ex);
            statusCode = 500;
            Error error = new Error();
            String message = "Something went wrong";
            error.setCode(statusCode.toString());
            error.setMessage(message);
            response = error;
        }
        
        System.out.println("Response: " + response);
        return new ResponseEntity<>(response, HttpStatus.valueOf(statusCode));
    }
    
    @Override
    public ResponseEntity<Object> deleteUser(
        @ApiParam(value = "Doctor id") @Valid @PathVariable(value = "doctorId", required = true) String doctorId
    ) {
        Object response; 
        Integer statusCode = 200;
        try {
            System.out.println("Request: " + doctorId);
            response = HospitalController.deleteUser(doctorId);
        } catch (HospitalCustomException e) {
            System.out.println("Custom Exception: " + e);
            statusCode = Integer.parseInt(e.getFaultObject().getCode());
            response = e.getFaultObject();
        } catch (Exception ex) {
            System.out.println("General exception: " + ex);
            statusCode = 500;
            Error error = new Error();
            String message = "Something went wrong";
            error.setCode(statusCode.toString());
            error.setMessage(message);
            response = error;
        }
        
        System.out.println("Response: " + response);
        return new ResponseEntity<>(response, HttpStatus.valueOf(statusCode));
    }
    
    @Override
    public ResponseEntity<Object> getUserId(
        @ApiParam(value = "The login body" ,required=true )  @Valid @RequestParam User user
    ) {                
        Object response; 
        Integer statusCode = 200;
        try {
            System.out.println("Request: " + user);
            response = HospitalController.getUserId(user);
        } catch (HospitalCustomException e) {
            System.out.println("Custom Exception: " + e);
            statusCode = Integer.parseInt(e.getFaultObject().getCode());
            response = e.getFaultObject();
        } catch (Exception ex) {
            System.out.println("General exception: " + ex);
            statusCode = 500;
            Error error = new Error();
            String message = "Something went wrong";
            error.setCode(statusCode.toString());
            error.setMessage(message);
            response = error;
        }
        
        System.out.println("Response: " + response);
        return new ResponseEntity<>(response, HttpStatus.valueOf(statusCode));
    }
    
    @Override
    public ResponseEntity<Object> getAvailableRooms(
        @ApiParam(value = "User id") @Valid @RequestParam(value = "user_id", required = false) String user_id,
        @ApiParam(value = "Date larger than") @Valid @RequestParam(value = "date_lt", required = false) String date_lt,
        @ApiParam(value = "Date greater than") @Valid @RequestParam(value = "date_gt", required = false) String date_gt
    ) {                
        Object response; 
        Integer statusCode = 200;
        try {
            System.out.println("Request: " + user_id);
            response = HospitalController.getAvailableRooms(user_id, date_lt, date_gt);
        } catch (HospitalCustomException e) {
            System.out.println("Custom Exception: " + e);
            statusCode = Integer.parseInt(e.getFaultObject().getCode());
            response = e.getFaultObject();
        } catch (Exception ex) {
            System.out.println("General exception: " + ex);
            statusCode = 500;
            Error error = new Error();
            String message = "Something went wrong";
            error.setCode(statusCode.toString());
            error.setMessage(message);
            response = error;
        }
        
        System.out.println("Response: " + response);
        return new ResponseEntity<>(response, HttpStatus.valueOf(statusCode));
    }
    
}
