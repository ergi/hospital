package com.hospital.Hospital.api;

import com.hospital.Hospital.models.Appointment;
import com.hospital.Hospital.models.Room;
import com.hospital.Hospital.models.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import javax.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author erginushi
 */

@Api(value = "appointment", description = "the Hospital API")
@RequestMapping("/hospital/v1")
public interface HospitalApi {
    
    @ApiOperation(value = "Creates an Appointment", nickname = "createAppointment", notes = "This operation creates a Appointment entity.", response = Appointment.class, tags={ "appointment", })
    @ApiResponses(value = { 
        @ApiResponse(code = 201, message = "Created", response = Appointment.class),
        @ApiResponse(code = 400, message = "Bad Request", response = Error.class),
        @ApiResponse(code = 401, message = "Unauthorized", response = Error.class),
        @ApiResponse(code = 403, message = "Forbidden", response = Error.class),
        @ApiResponse(code = 405, message = "Method Not allowed", response = Error.class),
        @ApiResponse(code = 409, message = "Conflict", response = Error.class),
        @ApiResponse(code = 500, message = "Internal Server Error", response = Error.class) })
    @PostMapping(value = "/appointment",
        produces = { "application/json" }, 
        consumes = { "application/json" }
    )
    ResponseEntity<Object> createAppointment(
        @ApiParam(value = "The Appointment to be created" ,required=true ) @Valid @RequestBody Appointment appointment
    );
    
    @ApiOperation(value = "Get an Appointment", nickname = "createAppointment", notes = "This operation creates a Appointment entity.", response = Appointment.class, tags={ "appointment", })
    @ApiResponses(value = { 
        @ApiResponse(code = 201, message = "Created", response = Appointment.class),
        @ApiResponse(code = 400, message = "Bad Request", response = Error.class),
        @ApiResponse(code = 401, message = "Unauthorized", response = Error.class),
        @ApiResponse(code = 403, message = "Forbidden", response = Error.class),
        @ApiResponse(code = 405, message = "Method Not allowed", response = Error.class),
        @ApiResponse(code = 409, message = "Conflict", response = Error.class),
        @ApiResponse(code = 500, message = "Internal Server Error", response = Error.class) })
    @GetMapping(value = "/appointment/{doctorId}",
        produces = { "application/json" }
    )
    ResponseEntity<Object> getAllAppointments(
           @ApiParam(value = "Doctor id") @Valid @RequestParam(value = "doctorId", required = false) String doctorId,
           @ApiParam(value = "Date larger than") @Valid @RequestParam(value = "date_lt", required = false) String date_lt,
           @ApiParam(value = "Date greater than") @Valid @RequestParam(value = "date_gt", required = false) String date_gt
    );
    
    @ApiOperation(value = "Get an Appointment", nickname = "createAppointment", notes = "This operation creates a Appointment entity.", response = Appointment.class, tags={ "appointment", })
    @ApiResponses(value = { 
        @ApiResponse(code = 201, message = "Created", response = Appointment.class),
        @ApiResponse(code = 400, message = "Bad Request", response = Error.class),
        @ApiResponse(code = 401, message = "Unauthorized", response = Error.class),
        @ApiResponse(code = 403, message = "Forbidden", response = Error.class),
        @ApiResponse(code = 405, message = "Method Not allowed", response = Error.class),
        @ApiResponse(code = 409, message = "Conflict", response = Error.class),
        @ApiResponse(code = 500, message = "Internal Server Error", response = Error.class) })
    @PutMapping(value = "/appointment/{doctorId}/{id}",
        produces = { "application/json" }
    )
    ResponseEntity<Object> deleteAppointment(
        @ApiParam(value = "Doctor id") @Valid @PathVariable(value = "doctorId", required = true) String doctorId,
        @ApiParam(value = "Appointment id") @Valid @PathVariable(value = "id", required = true) String appointmentId
    );
    
    @ApiOperation(value = "Creates an User", nickname = "createUser", notes = "This operation creates an User.", response = User.class, tags={ "appointment", })
    @ApiResponses(value = { 
        @ApiResponse(code = 201, message = "Created", response = User.class),
        @ApiResponse(code = 400, message = "Bad Request", response = Error.class),
        @ApiResponse(code = 401, message = "Unauthorized", response = Error.class),
        @ApiResponse(code = 403, message = "Forbidden", response = Error.class),
        @ApiResponse(code = 405, message = "Method Not allowed", response = Error.class),
        @ApiResponse(code = 409, message = "Conflict", response = Error.class),
        @ApiResponse(code = 500, message = "Internal Server Error", response = Error.class) })
    @PostMapping(value = "/create",
        produces = { "application/json" }, 
        consumes = { "application/json" }
    )
    ResponseEntity<Object> createUser(
        @ApiParam(value = "The User to be created" ,required=true )  @Valid @RequestBody User user
    );
    
    @ApiOperation(value = "Get all Users", nickname = "getAllUsers", notes = "This operation gets all Users.", response = User.class, tags={ "appointment", })
    @ApiResponses(value = { 
        @ApiResponse(code = 201, message = "Created", response = User.class),
        @ApiResponse(code = 400, message = "Bad Request", response = Error.class),
        @ApiResponse(code = 401, message = "Unauthorized", response = Error.class),
        @ApiResponse(code = 403, message = "Forbidden", response = Error.class),
        @ApiResponse(code = 405, message = "Method Not allowed", response = Error.class),
        @ApiResponse(code = 409, message = "Conflict", response = Error.class),
        @ApiResponse(code = 500, message = "Internal Server Error", response = Error.class) })
    @GetMapping(value = "/get",
        produces = { "application/json" }
    )
    ResponseEntity<Object> getAllUsers(
        @ApiParam(value = "The receptionist" ,required=true )  @Valid @RequestParam String user
    );
    
    @ApiOperation(value = "Delete a doctor", nickname = "deleteDoctor", notes = "This operation deletes a doctor entity.", response = User.class, tags={ "appointment", })
    @ApiResponses(value = { 
        @ApiResponse(code = 201, message = "Created", response = User.class),
        @ApiResponse(code = 400, message = "Bad Request", response = Error.class),
        @ApiResponse(code = 401, message = "Unauthorized", response = Error.class),
        @ApiResponse(code = 403, message = "Forbidden", response = Error.class),
        @ApiResponse(code = 405, message = "Method Not allowed", response = Error.class),
        @ApiResponse(code = 409, message = "Conflict", response = Error.class),
        @ApiResponse(code = 500, message = "Internal Server Error", response = Error.class) })
    @PutMapping(value = "/user/{doctorId}",
        produces = { "application/json" }
    )
    ResponseEntity<Object> deleteUser(
        @ApiParam(value = "Doctor id") @Valid @PathVariable(value = "doctorId", required = false) String doctorId
    );
    
    @ApiOperation(value = "Get all Users", nickname = "getAllUsers", notes = "This operation gets all Users.", response = User.class, tags={ "appointment", })
    @ApiResponses(value = { 
        @ApiResponse(code = 201, message = "Created", response = User.class),
        @ApiResponse(code = 400, message = "Bad Request", response = Error.class),
        @ApiResponse(code = 401, message = "Unauthorized", response = Error.class),
        @ApiResponse(code = 403, message = "Forbidden", response = Error.class),
        @ApiResponse(code = 405, message = "Method Not allowed", response = Error.class),
        @ApiResponse(code = 409, message = "Conflict", response = Error.class),
        @ApiResponse(code = 500, message = "Internal Server Error", response = Error.class) })
    @PostMapping(value = "/get",
        produces = { "application/json" },
        consumes = { "application/json" }
    )
    ResponseEntity<Object> getUserId(
        @ApiParam(value = "The login body" ,required=true )  @Valid @RequestParam User user
    );
    
    @ApiOperation(value = "Get all Users", nickname = "getAllUsers", notes = "This operation gets all Users.", response = Room.class)
    @ApiResponses(value = { 
        @ApiResponse(code = 201, message = "Created", response = Room.class),
        @ApiResponse(code = 400, message = "Bad Request", response = Error.class),
        @ApiResponse(code = 401, message = "Unauthorized", response = Error.class),
        @ApiResponse(code = 403, message = "Forbidden", response = Error.class),
        @ApiResponse(code = 405, message = "Method Not allowed", response = Error.class),
        @ApiResponse(code = 409, message = "Conflict", response = Error.class),
        @ApiResponse(code = 500, message = "Internal Server Error", response = Error.class) })
    @GetMapping(value = "/rooms",
        produces = { "application/json" }
    )
    ResponseEntity<Object> getAvailableRooms(
        @ApiParam(value = "User id") @Valid @RequestParam(value = "user_id", required = false) String user_id,
        @ApiParam(value = "Date larger than") @Valid @RequestParam(value = "date_lt", required = false) String date_lt,
        @ApiParam(value = "Date greater than") @Valid @RequestParam(value = "date_gt", required = false) String date_gt
    );
}
