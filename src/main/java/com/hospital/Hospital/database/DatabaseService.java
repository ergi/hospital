package com.hospital.Hospital.database;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hospital.Hospital.Exception.HospitalCustomException;
import com.hospital.Hospital.models.Appointment;
import com.hospital.Hospital.models.User;
import com.hospital.Hospital.models.Error;
import com.hospital.Hospital.models.Room;
import com.hospital.Hospital.utils.DataSource;
import com.hospital.Hospital.utils.Utils;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;


/**
 *
 * @author erginushi
 */
public class DatabaseService {
        
    public static Object createAppointment(Appointment appointment) throws HospitalCustomException, Exception{
        
        Appointment response = null;
        
        String appointmentStr = Utils.jsonToString(appointment);
        
        String procedure = "{ call createAppointment(?) } ";
        Object [] params = {
            appointmentStr
        };
        
        System.out.println("createAppointment(?): " + Arrays.toString(params));
        try (Connection connection = DataSource.getConnection(); PreparedStatement pstmt = setParams(connection, procedure, params); ResultSet rs = pstmt.executeQuery();){
            while (rs.next()) {
                String json = rs.getString("response");
                String validity = rs.getString("validity");
                System.out.println(json);
                if(Integer.parseInt(validity) == 1){
                    System.out.println("DB, validity: " + validity);
                    System.out.println("DB, response: " + json);
                    Error error = (Error) Utils.stringToJson(json, Error.class);
                    throw new HospitalCustomException(error);
                }
                ObjectMapper mapper = new ObjectMapper();
                
                response = mapper.readValue(json, new TypeReference<Appointment>(){});
                System.out.println("DB, response: " + response);
            }
        }
        
        return response;
    }
            
    public static Object getAllAppointments(String doctorId, String date_lt, String date_gt) throws HospitalCustomException, Exception{
        
        List<Appointment> response = null;
        
        String procedure = "{ call getAllAppointments(?,?,?) } ";
        Object [] params = {
            doctorId,
            date_lt,
            date_gt
        };
        System.out.println("getAllAppointments(?,?,?): " + Arrays.toString(params));
        try (Connection connection = DataSource.getConnection(); PreparedStatement pstmt = setParams(connection, procedure, params); ResultSet rs = pstmt.executeQuery();){
            
            while (rs.next()) {
                String json = rs.getString("response");
                String validity = rs.getString("validity");
                if(Integer.parseInt(validity) == 1){
                    System.out.println("DB, validity: " + validity);
                    System.out.println("DB, response: " + json);
                    Error error = (Error) Utils.stringToJson(json, Error.class);
                    throw new HospitalCustomException(error);
                }
                ObjectMapper mapper = new ObjectMapper();
                
                response = mapper.readValue(json, new TypeReference<List<Appointment>>(){});
                System.out.println("DB, response: " + response);
            }
        }
        
        
        return response;
    }
    
    public static Object deleteAppointment(String doctorId, String appointmentId) throws HospitalCustomException, Exception{
        
        Appointment response = null;
        
        String procedure = "{ call deleteAppointment(?,?) } ";
        Object [] params = {
            doctorId,
            appointmentId
        };
        
        System.out.println("deleteAppointment(?,?): " + Arrays.toString(params));
        try (Connection connection = DataSource.getConnection(); PreparedStatement pstmt = setParams(connection, procedure, params); ResultSet rs = pstmt.executeQuery();){
            
            while (rs.next()) {
                String json = rs.getString("response");
                String validity = rs.getString("validity");
                if(Integer.parseInt(validity) == 1){
                    System.out.println("DB, validity: " + validity);
                    System.out.println("DB, response: " + json);
                    Error error = (Error) Utils.stringToJson(json, Error.class);
                    throw new HospitalCustomException(error);
                }
                ObjectMapper mapper = new ObjectMapper();
                
                response = mapper.readValue(json, new TypeReference<Appointment>(){});
                System.out.println("DB, response: " + response);
            }
        }
        
        
        return response;
    }
    
    public static Object createUser(User user) throws HospitalCustomException, Exception{
        
        User response = null;
        String userStr = Utils.jsonToString(user);
        
        String procedure = "{ call createUser(?) } ";
        Object [] params = {
            userStr
        };
        
        System.out.println("createUser(?): " + Arrays.toString(params));
        try (Connection connection = DataSource.getConnection(); PreparedStatement pstmt = setParams(connection, procedure, params); ResultSet rs = pstmt.executeQuery();){
            
            while (rs.next()) {
                String json = rs.getString("response");
                String validity = rs.getString("validity");
                if(Integer.parseInt(validity) == 1){
                    System.out.println("DB, validity: " + validity);
                    System.out.println("DB, response: " + json);
                    Error error = (Error) Utils.stringToJson(json, Error.class);
                    throw new HospitalCustomException(error);
                }
                ObjectMapper mapper = new ObjectMapper();
                
                response = mapper.readValue(json, new TypeReference<User>(){});
                System.out.println("DB, response: " + response);
            }
        }
        
        
        return response;
    }
    
    public static Object getAllUsers(String user) throws HospitalCustomException, Exception{
        
        List<User> response = null;
        
        String procedure = "{ call getAllUsers(?) } ";
        Object [] params = {
            user
        };
        
        System.out.println("getAllUsers(?): " + Arrays.toString(params));
        try (Connection connection = DataSource.getConnection(); PreparedStatement pstmt = setParams(connection, procedure, params); ResultSet rs = pstmt.executeQuery();){
            
            while (rs.next()) {
                String json = rs.getString("response");
                String validity = rs.getString("validity");
                if(Integer.parseInt(validity) == 1){
                    System.out.println("DB, validity: " + validity);
                    System.out.println("DB, response: " + json);
                    Error error = (Error) Utils.stringToJson(json, Error.class);
                    throw new HospitalCustomException(error);
                }
                ObjectMapper mapper = new ObjectMapper();
                
                response = mapper.readValue(json, new TypeReference<List<User>>(){});
                System.out.println("DB, response: " + response);
            }
        }
        
        
        return response;
    }
    
    public static Object deleteUser(String doctorId) throws HospitalCustomException, Exception{
        
        User response = null;
        
        String procedure = "{ call deleteUser(?) } ";
        Object [] params = {
            doctorId
        };
        
        System.out.println("deleteUser(?): " + Arrays.toString(params));
        try (Connection connection = DataSource.getConnection(); PreparedStatement pstmt = setParams(connection, procedure, params); ResultSet rs = pstmt.executeQuery();){
            
            while (rs.next()) {
                String json = rs.getString("response");
                String validity = rs.getString("validity");
                if(Integer.parseInt(validity) == 1){
                    System.out.println("DB, validity: " + validity);
                    System.out.println("DB, response: " + json);
                    Error error = (Error) Utils.stringToJson(json, Error.class);
                    throw new HospitalCustomException(error);
                }
                ObjectMapper mapper = new ObjectMapper();
                
                response = mapper.readValue(json, new TypeReference<User>(){});
                System.out.println("DB, response: " + response);
            }
        }

        return response;
    }
    
    public static Object getUserId(User user) throws HospitalCustomException, Exception{
        
        User response = null;
        
        String userStr = Utils.jsonToString(user);
        String procedure = "{ call getUserID(?) } ";
        Object [] params = {
            userStr
        };
        
        System.out.println("getUserID(?): " + Arrays.toString(params));
        try (Connection connection = DataSource.getConnection(); PreparedStatement pstmt = setParams(connection, procedure, params); ResultSet rs = pstmt.executeQuery();){
            
            while (rs.next()) {
                String json = rs.getString("response");
                String validity = rs.getString("validity");
                if(Integer.parseInt(validity) == 1){
                    System.out.println("DB, validity: " + validity);
                    System.out.println("DB, response: " + json);
                    Error error = (Error) Utils.stringToJson(json, Error.class);
                    throw new HospitalCustomException(error);
                }
                ObjectMapper mapper = new ObjectMapper();
                
                response = mapper.readValue(json, new TypeReference<User>(){});
                System.out.println("DB, response: " + response);
            }
        }
        
        
        return response;
    }
    
    public static Object getAvailableRooms(String user_id, String date_lt, String date_gt) throws HospitalCustomException, Exception{
        
        List<Room> response = null;
        
        String procedure = "{ call getAvailableRooms(?,?,?) } ";
        Object [] params = {
            user_id,
            date_lt,
            date_gt
        };
        System.out.println("getAvailableRooms(?,?,?): " + Arrays.toString(params));
        try (Connection connection = DataSource.getConnection(); PreparedStatement pstmt = setParams(connection, procedure, params); ResultSet rs = pstmt.executeQuery();){
            
            while (rs.next()) {
                String json = rs.getString("response");
                String validity = rs.getString("validity");
                if(Integer.parseInt(validity) == 1){
                    System.out.println("DB, validity: " + validity);
                    System.out.println("DB, response: " + json);
                    Error error = (Error) Utils.stringToJson(json, Error.class);
                    throw new HospitalCustomException(error);
                }
                ObjectMapper mapper = new ObjectMapper();
                
                response = mapper.readValue(json, new TypeReference<List<Room>>(){});
                System.out.println("DB, response: " + response);
            }
        }
        
        return response;
    }
    
    public static PreparedStatement setParams(Connection connection, String procedure, Object[] params) throws SQLException{
        PreparedStatement pstmt = connection.prepareStatement(procedure);
        for(int i = 0; i < params.length; i++){
            pstmt.setObject(i + 1, params[i]);
        }
        
        return pstmt;
    }
}
