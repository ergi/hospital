package com.hospital.Hospital.controller;

import com.hospital.Hospital.Exception.HospitalCustomException;
import com.hospital.Hospital.database.DatabaseService;
import com.hospital.Hospital.models.Appointment;
import com.hospital.Hospital.models.User;

/**
 *
 * @author erginushi
 */
public class HospitalController {
    
    public static Object createAppointment(Appointment appointment) throws HospitalCustomException, Exception{
        
        Object response = DatabaseService.createAppointment(appointment);
        
        return response;
    }
            
    public static Object getAllAppointments(String doctorId, String date_lt, String date_gt) throws HospitalCustomException, Exception{
        
        Object response = DatabaseService.getAllAppointments(doctorId, date_lt, date_gt);
        
        return response;
    }
    
    public static Object deleteAppointment(String doctorId, String appointmentId) throws HospitalCustomException, Exception{
        
        Object response = DatabaseService.deleteAppointment(doctorId, appointmentId);
        
        return response;
    }
    
    public static Object createUser(User user) throws HospitalCustomException, Exception{
        
        Object response = DatabaseService.createUser(user);
        
        return response;
    }
    
    public static Object getAllUsers(String user) throws HospitalCustomException, Exception{
        
        Object response = DatabaseService.getAllUsers(user);
        
        return response;
    }
    
    public static Object deleteUser(String doctorId) throws HospitalCustomException, Exception{
        
        Object response = DatabaseService.deleteUser(doctorId);
        
        return response;
    }
    
    public static Object getUserId(User user) throws HospitalCustomException, Exception{
        
        Object response = DatabaseService.getUserId(user);
        
        return response;
    }
    
    public static Object getAvailableRooms(String user_id, String date_lt, String date_gt) throws HospitalCustomException, Exception{
        
        Object response = DatabaseService.getAvailableRooms(user_id, date_lt, date_gt);
        
        return response;
    }
}
