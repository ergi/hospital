/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital.Hospital.models;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author erginushi
 */
public class Appointment {
    
    @JsonProperty("appointment_id")
    private String appointment_id = null;
    
    @JsonProperty("doctor_id")
    private String doctor_id = null;
    
    @JsonProperty("receptionist_id")
    private String receptionist_id = null;
    
    @JsonProperty("patient_id")
    private String patient_id = null;
    
    @JsonProperty("room_id")
    private String room_id = null;
    
    @JsonProperty("start_date")
    private String start_date = null;
    
    @JsonProperty("end_date")
    private String end_date = null;

    public String getAppointment_id() {
        return appointment_id;
    }

    public void setAppointment_id(String appointment_id) {
        this.appointment_id = appointment_id;
    }

    public void setDoctor_id(String doctor_id) {
        this.doctor_id = doctor_id;
    }

    public void setReceptionist_id(String receptionist_id) {
        this.receptionist_id = receptionist_id;
    }

    public void setPatient_id(String patient_id) {
        this.patient_id = patient_id;
    }

    public void setRoom_id(String room_id) {
        this.room_id = room_id;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getDoctor_id() {
        return doctor_id;
    }

    public String getReceptionist_id() {
        return receptionist_id;
    }

    public String getPatient_id() {
        return patient_id;
    }

    public String getRoom_id() {
        return room_id;
    }

    public String getStart_date() {
        return start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    @Override
    public String toString() {
        return "Appointment{" + "appointment_id=" + appointment_id + ", doctor_id=" + doctor_id + ", receptionist_id=" + receptionist_id + ", patient_id=" + patient_id + ", room_id=" + room_id + ", start_date=" + start_date + ", end_date=" + end_date + '}';
    }
    
}
