/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital.Hospital.models;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author erginushi
 */
public class User {
    @JsonProperty("username")
    private String username = null;
    
    @JsonProperty("password")
    private String password = null;
    
    @JsonProperty("first_name")
    private String first_name = null;
    
    @JsonProperty("last_name")
    private String last_name = null;
    
    @JsonProperty("role")
    private String role = null;
    
    @JsonProperty("status")
    private String status = null;
    
    @JsonProperty("category")
    private String category = null;
    
    @JsonProperty("creation_date")
    private String creation_date = null;

    public String getPassword() {
        return password;
    }

    public String getFirst_name() {
        return first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public String getRole() {
        return role;
    }

    public String getStatus() {
        return status;
    }

    public String getCategory() {
        return category;
    }

    public String getCreation_date() {
        return creation_date;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setCreation_date(String creation_date) {
        this.creation_date = creation_date;
    }
    
    
    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
      this.username = username;
    } 

    @Override
    public String toString() {
        return "User{" + "username=" + username + ", password=" + password + ", first_name=" + first_name + ", last_name=" + last_name + ", role=" + role + ", status=" + status + ", category=" + category + ", creation_date=" + creation_date + '}';
    }
    
}
