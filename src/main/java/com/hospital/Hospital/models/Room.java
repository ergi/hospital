/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital.Hospital.models;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author erginushi
 */
public class Room {
    @JsonProperty("id")
    private String id = null;
    
    @JsonProperty("room_no")
    private String room_no = null;
    
    @JsonProperty("description")
    private String description = null;
    
    @JsonProperty("created_by")
    private String created_by = null;
    
    @JsonProperty("capacity")
    private String capacity = null;
    
    @JsonProperty("status")
    private String status = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRoom_no() {
        return room_no;
    }

    public void setRoom_no(String room_no) {
        this.room_no = room_no;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public String getCapacity() {
        return capacity;
    }

    public void setCapacity(String capacity) {
        this.capacity = capacity;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Room{" + "id=" + id + ", room_no=" + room_no + ", description=" + description + ", created_by=" + created_by + ", capacity=" + capacity + ", status=" + status + '}';
    }
    
}
