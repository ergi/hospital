FROM openjdk:8-jdk
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} Hospital-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","Hospital-0.0.1-SNAPSHOT.jar"]
